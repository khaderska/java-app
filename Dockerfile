FROM tomcat:8.0-alpine

LABEL maintainer="Khader Mohideen"

ADD target/helloworld.war /usr/local/tomcat/webapps/

EXPOSE 8080

CMD ["catalina.sh", "run"]
